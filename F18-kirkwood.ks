# Build a minimal Fedora ARM (Kirkwood) image using livemedia-creator

lang en_US.UTF-8
keyboard us
timezone --utc US/Eastern
auth --useshadow --enablemd5
selinux --enforcing
firewall --enabled --service=mdns,ssh
network --bootproto=dhcp --device=eth0 --onboot=on --activate --hostname=localhost.localdomain
services --enabled=NetworkManager,sshd,chronyd --disabled=network

skipx

# Set a default root password for Fedora
rootpw --plaintext fedora

# Repositories
# apparently we must use 'url' for the install repo for livemedia-creator
url --url="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Everything/arm/os/"

#
# Define how large you want your rootfs to be
#
# NOTE: /boot and swap MUST use --asprimary to ensure '/' is 
#       the last partition in order for rootfs-resize to work.
#
bootloader --location=none
zerombr
clearpart --all
part /boot --size 200 --fstype ext2 --asprimary --label=boot
part swap --size 500 --asprimary --label=swap
part / --size 2000 --fstype ext4 --label=rootfs

#
# Add all the packages after the base packages
#
%packages --nobase
@standard

# apparently none of the groups sets the clock.
chrony

# and ifconfig would be nice.
net-tools

# we'll want to resize the rootfs on first boot
rootfs-resize

# get the uboot tools
uboot-tools

%end


# more configuration
%post --erroronfail


# set up the U-Boot config for Kirkwood
#use defaults, for now.

# Install U-Boot boot.scr
pushd /boot

# get the root device from fstab, typically UUID=<string>
ROOTDEV=`grep -w / /etc/fstab | cut -d ' ' -f1`

# setup boot.scr

KERNEL_ADDR=0x6400000
INITRD_ADDR=0x7400000

cat <<EOL > boot.cmd.mmc
setenv bootargs console=ttyS0,115200n8 ro root=$ROOTDEV rootwait
ext2load mmc 0:1 $INITRD_ADDR /uInitrd
ext2load mmc 0:1 $KERNEL_ADDR /uImage
bootm $KERNEL_ADDR $INITRD_ADDR
EOL

sed -e 's/mmc/usb/g' boot.cmd.mmc > boot.cmd.usb

/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Kirkwood F18" -d boot.cmd.mmc boot.scr.mmc
/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Kirkwood F18" -d boot.cmd.usb boot.scr.usb

cp -p boot.scr.mmc boot.scr


popd


# force resize of the rootfs
touch /.rootfs-repartition

# try Brendan's tip for workaround.
setfiles -v -F -e /proc -e /sys -e /dev \
  /etc/selinux/targeted/contexts/files/file_contexts /


%end

