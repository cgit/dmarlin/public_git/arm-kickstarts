# Build an XFCE Fedora ARM (VExpress) Soft-FP image using livemedia-creator

lang en_US.UTF-8
keyboard us
timezone --utc US/Eastern
auth --useshadow --enablemd5
selinux --enforcing
firstboot --enable
firewall --enabled --service=mdns,ssh
network --bootproto=dhcp --device=eth0 --onboot=on --activate --hostname=localhost.localdomain
services --enabled=NetworkManager,sshd,chronyd --disabled=network

# Set a default root password for Fedora
rootpw --plaintext fedora

# Repositories
# apparently we must use 'url' for the install repo for livemedia-creator
url --url="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Fedora/arm/os/"
repo --name=fedora --baseurl="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Everything/arm/os/"

#
# Define how large you want your rootfs to be
#
# NOTE: /boot and swap MUST use --asprimary to ensure '/' is 
#       the last partition in order for rootfs-resize to work.
#
bootloader --location=none
zerombr
clearpart --all
part /boot --size 200 --fstype ext3 --label=boot
part swap --size 500 --asprimary --label=swap
part / --size 2200 --fstype ext4 --label=rootfs

#
# Add all the packages after the base packages
#
%packages --nobase
@standard

# get the base kernel
kernel

# vvvvvvvvvvvvvvvvvvvvvvv
@base-x
@fonts

@xfce-desktop
@xfce-apps
@xfce-extra-plugins
@xfce-media

# lm_sensors cause warnings on ARM systems
#-lm_sensors
-xfce4-sensors-plugin

# apparently, xfce-desktop does not include gdm or a theme (icons)
gdm
xfce4-icon-theme

# First, no office
-libreoffice-*
-planner

# Drop the Java plugin
-icedtea-web
-java-1.6.0-openjdk

# save some space
-autofs
-acpid

# Remove default unwanted hardware firmware and support we don't want
-foomatic*
-ghostscript*
-ivtv-firmware
# These are listed somewhere other than hardware support!
-irda-utils
-fprintd*

-hplip
-hpijs

# Dictionaries are big
-aspell-*
-hunspell-*
-man-pages*
-words

# scanning takes quite a bit of space :/
-xsane
-xsane-gimp
-sane-backends

# qlogic firmwares
-ql2100-firmware
-ql2200-firmware
-ql23xx-firmware
-ql2400-firmware

#-xfburn  appears twice in the menu, but the same behavior in PA

# ^^^^^^^^^^^^^^^^^^^^^^^

# apparently none of the groups sets the clock.
chrony

# and ifconfig would be nice.
net-tools

# get the uboot tools
uboot-tools

%end


# more configuration
%post --erroronfail

# remove the extraneous kernel
yum -y remove kernel-kirkwood


# vvvvvvvvvvvvvvvvvvvvvvv
# xfce configuration

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/startxfce4
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

mkdir -p /root/.config/xfce4

cat > /root/.config/xfce4/helpers.rc << FOE
MailReader=sylpheed-claws
FileManager=Thunar
FOE

# disable screensaver locking (#674410)
cat >> /root/.xscreensaver << FOE
mode:           off
lock:           False
dpmsEnabled:    False
FOE

# deactivate xfconf-migration (#683161)
rm -f /etc/xdg/autostart/xfconf-migration-4.6.desktop || :

# deactivate xfce4-panel first-run dialog (#693569)
mkdir -p /root/.config/xfce4/xfconf/xfce-perchannel-xml
cp /etc/xdg/xfce4/panel/default.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

# make sure the default target is graphical
ln -sf /lib/systemd/system/graphical.target /etc/systemd/system/default.target
ln -sf /lib/systemd/system/graphical.target /lib/systemd/system/default.target

# firstboot does not run for us, so force it
/bin/systemctl enable firstboot-graphical.service

# ^^^^^^^^^^^^^^^^^^^^^^^


# try Brendan's tip for workaround.
setfiles -v -F -e /proc -e /sys -e /dev \
  /etc/selinux/targeted/contexts/files/file_contexts /


%end


# get the scripts to run qemu
%post --nochroot

pushd /mnt/sysimage/boot

wget "http://dmarlin.fedorapeople.org/fedora-arm/vexpress/boot-vexpress"

chmod +x /mnt/sysimage/boot/boot-vexpress*

popd


%end

