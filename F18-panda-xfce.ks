# Build an XFCE Fedora ARM (OMAP) PandaBoard image using livemedia-creator

lang en_US.UTF-8
keyboard us
timezone --utc US/Eastern
auth --useshadow --enablemd5
selinux --enforcing
firstboot --enable
firewall --enabled --service=mdns,ssh
network --bootproto=dhcp --device=eth0 --onboot=on --activate --hostname=localhost.localdomain
services --enabled=NetworkManager,sshd,chronyd --disabled=network

# Set a default root password for Fedora
rootpw --plaintext fedora

# Repositories
# apparently we must use 'url' for the install repo for livemedia-creator
url --url="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Fedora/armhfp/os/"
repo --name=fedora --baseurl="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Everything/armhfp/os/"

#
# Define how large you want your rootfs to be
#
# NOTE: /boot and swap MUST use --asprimary to ensure '/' is 
#       the last partition in order for rootfs-resize to work.
#
bootloader --location=none
zerombr
clearpart --all
part /boot/uboot --size 200 --fstype vfat --label=uboot
part swap --size 500 --label=swap
part / --size 2200 --fstype ext4 --label=rootfs

#
# Add all the packages after the base packages
#
%packages --nobase
@standard

# vvvvvvvvvvvvvvvvvvvvvvv
@base-x
@fonts

@xfce-desktop
@xfce-apps
@xfce-extra-plugins
@xfce-media

# lm_sensors cause warnings on ARM systems
#-lm_sensors
-xfce4-sensors-plugin

# apparently, xfce-desktop does not include gdm or a theme (icons)
gdm
xfce4-icon-theme

# First, no office
-libreoffice-*
-planner

# Drop the Java plugin
-icedtea-web
-java-1.6.0-openjdk

# save some space
-autofs
-acpid

# Remove default unwanted hardware firmware and support we don't want
-foomatic*
-ghostscript*
-ivtv-firmware
# These are listed somewhere other than hardware support!
-irda-utils
-fprintd*

-hplip
-hpijs

# Dictionaries are big
-aspell-*
-hunspell-*
-man-pages*
-words

# scanning takes quite a bit of space :/
-xsane
-xsane-gimp
-sane-backends

# qlogic firmwares
-ql2100-firmware
-ql2200-firmware
-ql23xx-firmware
-ql2400-firmware

#-xfburn  appears twice in the menu, but the same behavior in PA

# ^^^^^^^^^^^^^^^^^^^^^^^

# apparently none of the groups sets the clock.
chrony

# and ifconfig would be nice.
net-tools

# we'll want to resize the rootfs on first boot
rootfs-resize

# get the uboot tools
uboot-tools
# get MLO and uboot.bin 
uboot-panda


%end


# more configuration
%post --erroronfail

# vvvvvvvvvvvvvvvvvvvvvvv
# xfce configuration

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/startxfce4
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

mkdir -p /root/.config/xfce4

cat > /root/.config/xfce4/helpers.rc << FOE
MailReader=sylpheed-claws
FileManager=Thunar
FOE

# disable screensaver locking (#674410)
cat >> /root/.xscreensaver << FOE
mode:           off
lock:           False
dpmsEnabled:    False
FOE

# deactivate xfconf-migration (#683161)
rm -f /etc/xdg/autostart/xfconf-migration-4.6.desktop || :

# deactivate xfce4-panel first-run dialog (#693569)
mkdir -p /root/.config/xfce4/xfconf/xfce-perchannel-xml
cp /etc/xdg/xfce4/panel/default.xml /root/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

# make sure the default target is graphical
ln -sf /lib/systemd/system/graphical.target /etc/systemd/system/default.target
ln -sf /lib/systemd/system/graphical.target /lib/systemd/system/default.target

# firstboot does not run for us, so force it
/bin/systemctl enable firstboot-graphical.service

# force xorg to use framebuffer until we get omapdrm working well
cat > /etc/X11/xorg.conf.d/arm-fbdev.conf << EOF
Section "Device"
    Identifier             "Device0"
    Driver                 "fbdev"	# Choose the driver used for this monitor
EndSection
EOF

# ^^^^^^^^^^^^^^^^^^^^^^^


# set up the U-Boot config for Panda
cat << EOF >> /etc/sysconfig/uboot
UBOOT_DEVICE=mmcblk0p1
UBOOT_DIR=/boot/uboot
EOF


# Set up the bootloader bits on the U-Boot partition
pushd /boot

# MLO _must_ be the first thing copied to the partition
cp -p /usr/share/uboot-panda/MLO /boot/uboot/.

sleep 3
sync

# try to make damned sure that MLO is written first!
sleep 3
sync; sync; sync;

# now copy the bootloader
cp -p /usr/share/uboot-panda/u-boot.* /boot/uboot/.

# and move the U-Boot kernel and initrd images
mv boot.cmd uImage* uInitrd* uEnv* /boot/uboot/.


# Install U-Boot boot script and environment
pushd uboot

# get the root device from fstab, typically UUID=<string>
ROOTDEV=`grep -w / /etc/fstab | cut -d ' ' -f1`
KERNEL_ADDR=0x80300000
INITRD_ADDR=0x81600000

# setup uEnv.txt
cat <<EOL > uEnv.txt
mmcargs=setenv bootargs console=\${console} vram=\${vram} root=$ROOTDEV ro rootwait quiet rhgb
mmcload=fatload mmc 0:1 $INITRD_ADDR uInitrd; fatload mmc 0:1 $KERNEL_ADDR uImage; 
loaduimage=run mmcload; run mmcargs; bootm $KERNEL_ADDR $INITRD_ADDR
EOL

# and boot.scr
cat <<EOL > boot.cmd
setenv bootargs console=\${console} vram=\${vram} root=$ROOTDEV ro rootwait quiet rhgb
fatload mmc 0:1 $INITRD_ADDR uInitrd
fatload mmc 0:1 $KERNEL_ADDR uImage
bootm $KERNEL_ADDR $INITRD_ADDR
EOL

/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Panda F18" -d boot.cmd boot.scr

popd

popd


# force resize of the rootfs
touch /.rootfs-repartition

# try Brendan's tip for workaround.
setfiles -v -F -e /proc -e /sys -e /dev \
  /etc/selinux/targeted/contexts/files/file_contexts /


%end

