# Build a minimal Fedora ARM (Tegra) Trim Slice image using livemedia-creator

lang en_US.UTF-8
keyboard us
timezone --utc US/Eastern
auth --useshadow --enablemd5
selinux --enforcing
firewall --enabled --service=mdns,ssh
network --bootproto=dhcp --device=eth0 --onboot=on --activate --hostname=localhost.localdomain 
services --enabled=NetworkManager,sshd,chronyd --disabled=network

skipx

# Set a default root password for Fedora
rootpw --plaintext fedora

# Repositories
# apparently we must use 'url' for the install repo for livemedia-creator
url --url="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Fedora/arm/os/"
repo --name=fedora --baseurl="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Everything/arm/os/"

#
# Define how large you want your rootfs to be
#
# NOTE: /boot and swap MUST use --asprimary to ensure '/' is 
#       the last partition in order for rootfs-resize to work.
#
bootloader --location=none
zerombr
clearpart --all
part /boot --size 200 --fstype ext3 --label=boot
part swap --size 500 --asprimary --label=swap
part / --size 2000 --fstype ext4 --label=rootfs

#
# Add all the packages after the base packages
#
%packages --nobase
@standard

# apparently none of the groups sets the clock.
chrony

# and ifconfig would be nice.
net-tools

# we'll want to resize the rootfs on first boot
rootfs-resize

# get the uboot tools
uboot-tools

%end


# more configuration
%post --erroronfail


# Install U-Boot boot.scr
pushd /boot

# get the root device from fstab, typically UUID=<string>
ROOTDEV=`grep -w / /etc/fstab | cut -d ' ' -f1`

# setup boot.scr files
# NOTE: 
# If the .dtb file does not exist, an error will be displayed,
#   but the script will continue.
# If running the old (original) version of U-Boot, 
#   the third address to bootm will be ignored.
cat <<EOL > boot.cmd.mmc
setenv bootargs console=\${console} root=$ROOTDEV ro rootwait
ext2load mmc 0:1 4880000 uInitrd
ext2load mmc 0:1 4080000 uImage
ext2load mmc 0:1 5880000 dtbs/tegra20-trimslice.dtb
bootm 4080000 4880000 5880000
EOL

sed -e 's/mmc/usb/g' boot.cmd.mmc > boot.cmd.usb

/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Trim Slice F18" -d boot.cmd.mmc boot.scr.mmc
/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Trim Slice F18" -d boot.cmd.usb boot.scr.usb

cp -p boot.scr.mmc boot.scr

popd


# force resize of the rootfs
touch /.rootfs-repartition

# try Brendan's tip for workaround.
setfiles -v -F -e /proc -e /sys -e /dev \
  /etc/selinux/targeted/contexts/files/file_contexts /


%end

