# Perform a basic Fedora 18 ARM Hard-FP install for 
#   Calxeda EnergyCore ECX-1000 - HighBank

lang en_US.UTF-8
keyboard us
timezone --utc US/Eastern
auth --useshadow --enablemd5
selinux --enforcing
firewall --enabled --service=mdns,ssh
network --bootproto=dhcp --device=eth0 --onboot=on --activate --hostname=localhost.localdomain
services --enabled=NetworkManager,sshd,chronyd --disabled=network

# Only use this with Anaconda for serial console installs, not with livemedia-creator.
#cmdline
text

# no GUI on this one
skipx

# Set a default root password for Fedora
rootpw --plaintext fedora

# Repositories
# apparently we must use 'url' for the install repo for livemedia-creator
url --url="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Fedora/armhfp/os/"
repo --name=fedora --baseurl="http://dl.fedoraproject.org/pub/fedora-secondary/releases/18/Everything/armhfp/os/"

#
# Define how large you want your rootfs to be
#
bootloader --location=partition
zerombr
clearpart --all
part /boot --size 500 --fstype ext3 --label=boot
part swap --size 4000 --label=swap
part / --size 5000 --grow --fstype ext4 --label=rootfs


# Reboot after the installation is complete.
reboot


#
# Add all the packages after the base packages
#
%packages
@standard

# apparently none of the groups sets the clock.
chrony

# and ifconfig would be nice.
net-tools

%end


# more configuration
%post --erroronfail

# Install U-Boot boot.scr
pushd /boot

# get the root device from fstab, typically UUID=<string>
ROOTDEV=`grep -w / /etc/fstab | cut -d ' ' -f1`

# setup boot.scr
cat <<EOL > boot.cmd
setenv bootargs console=ttyAMA0 root=$ROOTDEV ro rootwait
ext2load scsi 0:1 \${ramdisk_addr_r} uInitrd
ext2load scsi 0:1 \${kernel_addr_r} uImage
bootm \${kernel_addr_r} \${ramdisk_addr_r} \${fdt_addr}
EOL

/usr/bin/mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "HighBank F18" -d boot.cmd boot.scr

popd


%end

